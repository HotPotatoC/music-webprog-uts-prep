<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Genre;



class HomeController extends Controller
{
    public function index()
    {
        $albums = Album::paginate(6);
        $genres = Genre::all();
        return view('home', compact('albums', 'genres'));
    }
}
