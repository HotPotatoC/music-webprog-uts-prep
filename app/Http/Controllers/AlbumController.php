<?php

namespace App\Http\Controllers;

use App\Models\Album;

class AlbumController extends Controller
{
    public function show(Album $album)
    {
        $paginatedReviews = $album->reviews()->paginate(5);

        $overallRating = 0;
        foreach ($album->reviews as $review) {
            $overallRating += $review->rating;
        }
        $overallRating = round($overallRating / count($album->reviews), 2);

        return view('albums.details', compact('album', 'paginatedReviews', 'overallRating'));
    }
}
