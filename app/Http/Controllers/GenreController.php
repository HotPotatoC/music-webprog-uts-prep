<?php

namespace App\Http\Controllers;

use App\Models\Genre;

class GenreController extends Controller
{
    public function index(Genre $genre)
    {
        return view('genres.index', compact('genre'));
    }
}
