<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("albums")->insert([
            "title" => "Ants From Up There",
            "cover_art_url" => "bcnr-ants-from-up-there.jpg",
            "artist" => "Black Country, New Road",
            "year" => 2022,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "Blonde",
            "cover_art_url" => "frank-ocean-blonde.jpg",
            "artist" => "Frank Ocean",
            "year" => 2016,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "Details",
            "cover_art_url" => "frou-frou-details.jpg",
            "artist" => "Frou Frou",
            "year" => 2002,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "good kid, m.A.A.d city",
            "cover_art_url" => "kendrick-lamar-good-kid-m_a_a_d-city.jpg",
            "artist" => "Kendrick Lamar",
            "year" => 2012,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "To Pimp a Butterfly",
            "cover_art_url" => "kendrick-lamar-to-pimp-a-butterfly.jpg",
            "artist" => "Kendrick Lamar",
            "year" => 2015,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "恋人へ (Koibito e)",
            "cover_art_url" => "lamp-koibito-e.jpg",
            "artist" => "Lamp",
            "year" => 2004,
            "language" => "Japanese"
        ]);

        DB::table("albums")->insert([
            "title" => "ランプ幻想 (Lamp Gensō)",
            "cover_art_url" => "lamp-lamp-genso.jpg",
            "artist" => "Lamp",
            "year" => 2008,
            "language" => "Japanese"
        ]);

        DB::table("albums")->insert([
            "title" => "What's Going On",
            "cover_art_url" => "marvin-gaye-whats-going-on.jpg",
            "artist" => "Marvin Gaye",
            "year" => 1971,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "Loveless",
            "cover_art_url" => "mbv-loveless.jpg",
            "artist" => "My Bloody Valentine",
            "year" => 1991,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "Modal Soul",
            "cover_art_url" => "nujabes-modal-soul.jpg",
            "artist" => "Nujabes",
            "year" => 2005,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "In Rainbows",
            "cover_art_url" => "radiohead-in-rainbows.jpg",
            "artist" => "Radiohead",
            "year" => 2007,
            "language" => "English"
        ]);

        DB::table("albums")->insert([
            "title" => "OK Computer",
            "cover_art_url" => "radiohead-ok-computer.jpg",
            "artist" => "Radiohead",
            "year" => 1997,
            "language" => "English"
        ]);
    }
}
