<?php

namespace Database\Seeders;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $albumIDs = DB::table("albums")->pluck("id");
        foreach ($albumIDs as $albumID) {
            for ($i = 0; $i < rand(10, 25); $i++) {
                DB::table("reviews")->insert([
                    'album_id' => $albumID,
                    'username' => fake()->userName,
                    'rating' => fake()->numberBetween(0, 10),
                    'comment' => fake()->paragraph(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }
}
