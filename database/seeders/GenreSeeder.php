<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("genres")->insert([
            ["name" => "Chamber Pop"] /* 1 */,
            ["name" => "Jazz Pop"] /* 2 */,
            ["name" => "Folk Pop"] /* 3 */,
            ["name" => "Noise Pop"] /* 4 */,
            ["name" => "Art Pop"] /* 5 */,
            ["name" => "Art Rock"] /* 6 */,
            ["name" => "Alternative Rock"] /* 7 */,
            ["name" => "Post-Rock"] /* 8 */,
            ["name" => "Indie Rock"] /* 9 */,
            ["name" => "Indietronica"] /* 10 */,
            ["name" => "Downtempo"] /* 11 */,
            ["name" => "Soul"] /* 12 */,
            ["name" => "Neo-Soul"] /* 13 */,
            ["name" => "West Coast Hip Hop"] /* 14 */,
            ["name" => "Conscious Hip Hop"] /* 15 */,
            ["name" => "Instrumental Hip Hop"] /* 16 */,
            ["name" => "Jazz Rap"] /* 17 */,
            ["name" => "Shibuya-Kei"] /* 18 */,
            ["name" => "Shoegaze"] /* 19 */,
            ["name" => "Alternative R&B"] /* 20 */,
        ]);

        DB::table("album_genre")->insert([
            ["album_id" => 1, "genre_id" => 1],
            ["album_id" => 1, "genre_id" => 6],
            ["album_id" => 1, "genre_id" => 8],
            ["album_id" => 1, "genre_id" => 9],

            ["album_id" => 2, "genre_id" => 20],
            ["album_id" => 2, "genre_id" => 5],
            ["album_id" => 2, "genre_id" => 13],

            ["album_id" => 3, "genre_id" => 5],
            ["album_id" => 3, "genre_id" => 10],
            ["album_id" => 3, "genre_id" => 11],

            ["album_id" => 4, "genre_id" => 14],
            ["album_id" => 4, "genre_id" => 15],

            ["album_id" => 5, "genre_id" => 14],
            ["album_id" => 5, "genre_id" => 15],
            ["album_id" => 5, "genre_id" => 17],

            ["album_id" => 6, "genre_id" => 2],
            ["album_id" => 6, "genre_id" => 18],

            ["album_id" => 7, "genre_id" => 2],
            ["album_id" => 7, "genre_id" => 3],
            ["album_id" => 7, "genre_id" => 18],

            ["album_id" => 8, "genre_id" => 12],

            ["album_id" => 9, "genre_id" => 4],
            ["album_id" => 9, "genre_id" => 19],

            ["album_id" => 10, "genre_id" => 16],
            ["album_id" => 10, "genre_id" => 17],

            ["album_id" => 11, "genre_id" => 6],
            ["album_id" => 11, "genre_id" => 7],

            ["album_id" => 12, "genre_id" => 6],
            ["album_id" => 12, "genre_id" => 7],
        ]);
    }
}
