<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield("title")</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-secondary">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">RYM</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link @if(url()->current() == route('home'))active @endif" aria-current="page"
                            href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(url()->current() == route('home'))active @endif" aria-current="page"
                            href="#">Albums</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(url()->current() == route('home'))active @endif" aria-current="page"
                            href="#">Genres</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @yield("content")
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>

</html>
