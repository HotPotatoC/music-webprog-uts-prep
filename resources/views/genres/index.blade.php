@extends("layouts.base")

@section("title", $genre->name . " Albums | RateYourMusic")

@section("content")
<div class="container mt-5">
    <div class="row mb-4">
        <div class="col">
            <h1 class="display-1">{{ $genre->name }} Albums</h1>
        </div>
    </div>
    <div class="row">
        @forelse ($genre->albums as $album)
        <div class="col-4 mb-5">
            <div class="card shadow-sm">
                <img class="card-img-top" src="{{ asset('cover_arts/'.$album->cover_art_url) }}" alt="" />
                <div class="card-body">
                    <h3 class="card-title">{{ $album->title }} ({{ $album->year }})</h3>
                    <h4 class="card-subtitle text-secondary">{{ $album->artist }}</h4>
                    <div class="d-flex justify-content-end">
                        <a class="btn btn-lg btn-success mt-2" href="{{ route('albums.details', $album->id) }}">
                            View
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <div class="col">
            <p>No Albums...</p>
        </div>
        @endforelse
    </div>
</div>
@endsection
