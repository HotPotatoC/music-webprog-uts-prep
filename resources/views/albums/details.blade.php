@extends("layouts.base")
@section("title", $album->title . " | RateYourMusic")

@section("content")
<div class="container mt-5">
    <div class="row border-bottom pb-4 mb-2">
        <div class="col-4">
            <img class="mw-100" src="{{ asset('cover_arts/' . $album->cover_art_url) }}" alt="" />
        </div>
        <div class="col-8">
            <div class="d-flex align-items-baseline">
                <h1 class="display-1 mb-2 me-2">{{ $album->title }}</h1>
                <h3 class="text-secondary">({{$album->year}})</h3>
            </div>
            <h2 class="text-secondary mb-2">{{ $album->artist }}</h2>
            <div class="d-flex align-items-baseline">
                <h1 class="mb-2 me-2">{{ $overallRating }}/10.00</h1>
                <h3 class="text-secondary">({{$album->year}})</h3>
            </div>
            <div class="d-flex flex-wrap mb-2">
                @forelse ($album->genres as $genre)
                <a href="{{ route('genres.index', $genre->id) }}">
                    <h5 class="me-2">
                        <span class="badge bg-secondary rounded-pill px-4 py-2">{{ $genre->name }}</span>
                    </h5>
                </a>
                @empty
                <p>No Genres...</p>
                @endforelse
            </div>
            <h5 class="me-2">
                <span class="badge bg-primary rounded-pill px-4 py-2">{{ $album->language }}</span>
            </h5>
        </div>
    </div>
    <h1>{{count($album->reviews)}} Reviews</h1>
    <div class="row">
        <div class="col">{{$paginatedReviews->links()}}</div>
    </div>
    @forelse ($paginatedReviews as $review)
    <div class="row border-bottom py-4">
        <div class="col-4">
            <h4>{{ $review->username }}</h4>
            <h5 class="text-primary">{{ $review->rating }}/10</h5>
        </div>
        <div class="col-8">
            <p>{{ $review->comment }}</p>
        </div>
    </div>
    @empty
    <div class="row">
        <div class="col-4">
            <p>No Reviews...</p>
        </div>
    </div>
    @endforelse
    <div class="row mt-2 mb-5">
        <div class="col">{{$paginatedReviews->links()}}</div>
    </div>
</div>
@endsection
